import { Component, OnChanges,OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Knight} from './knight.interface';
import {KnightService} from './knight.service'
import 'rxjs/add/operator/pluck'
@Component({
  selector: 'app-knight',
  templateUrl: './knight.component.html',
  styleUrls: ['./knight.component.css']
})
export class KnightComponent implements OnChanges,OnInit {
  knight:Knight;

  constructor(private route: ActivatedRoute,private knightService:KnightService) { }

  ngOnInit() {
  this.route.data.pluck('knight').subscribe((knight:Knight)=>{
    this.knight = knight;
  this.knightService.choseKnight(this.knight)}
    );


  }
  ngOnChanges(){

  }

}
