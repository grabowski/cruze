import { Injectable } from '@angular/core';
import {CanActivateChild,ActivatedRouteSnapshot} from '@angular/router';
import {KnightService} from './knight.service'


@Injectable()
export class KnightsGuardService implements CanActivateChild{

constructor(private knightsService:KnightService){

}
  canActivateChild(childRoute:ActivatedRouteSnapshot,state){

   if( this.knightsService.chosenKnight.name.toLowerCase() !== "patsy") {
     return true
   } else {
     console.log('Patsy is not a knight');
     return false
   }


  }

}
