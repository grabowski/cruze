export interface Knight {
  name:string;
  quest:string;
  color:string;
  isBrave:boolean;
}
