import { TestBed, inject } from '@angular/core/testing';

import { KnightsGuardService } from './knights-guard.service';

describe('KnightsGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KnightsGuardService]
    });
  });

  it('should ...', inject([KnightsGuardService], (service: KnightsGuardService) => {
    expect(service).toBeTruthy();
  }));
});
