import { Injectable } from '@angular/core';
import {Knight} from './knight.interface';

@Injectable()
export class KnightService {
  chosenKnight: Knight;

  private knights:Knight[] = [
    { name:'Lancelot',
      color:'blue',
      quest:'Seek holy grail',
      isBrave:true},
    { name:'Galahad',
      color:'blue',
      quest:'Find holy grail',
      isBrave:true
    },
    { name:'Gawain',
      color:'black',
      quest:'Find holy grail',
      isBrave:false
    },
  { name:'Tristan',
      color:'pink',
      quest:'Find Isolde',
      isBrave:true
    },
    {name:'Patsy',color:'gray',quest:'Is not a knight',isBrave:true}
  ]

getKnights(){
  return this.knights
}
getKnightByName(name:string){
return this.knights.filter(k=>k.name.toLowerCase()===name.toLowerCase())[0]
}
choseKnight(knight){
  console.log(knight)
  this.chosenKnight=knight;
}
}
