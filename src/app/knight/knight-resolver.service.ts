import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {KnightService} from './knight.service';
import {Knight} from './knight.interface';
import {ActivatedRouteSnapshot}from '@angular/router';

@Injectable()
export class KnightResolverService {
  constructor(private knightService: KnightService){}

  resolve(routeSnapshot:ActivatedRouteSnapshot):Knight{
    return this.knightService.getKnightByName(routeSnapshot.params['knight']);
  }

}
