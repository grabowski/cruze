import { TestBed, inject } from '@angular/core/testing';

import { BraveGuardService } from './brave-guard.service';

describe('BraveGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BraveGuardService]
    });
  });

  it('should ...', inject([BraveGuardService], (service: BraveGuardService) => {
    expect(service).toBeTruthy();
  }));
});
