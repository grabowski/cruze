import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KnightComponent} from './knight/knight.component';
import {KnightResolverService} from './knight/knight-resolver.service';
import {KnightsGuardService} from './knight/knights-guard.service';
import {BraveGuardService} from './brave-guard.service';

const routes: Routes = [
  {
    path: 'knight/:knight',
    component: KnightComponent,
    resolve:{
      knight: KnightResolverService
    },

    children:[
      {path:'goto',
                    canActivateChild:[KnightsGuardService],
      children:[
        {path:'bridge',
          loadChildren:'app/places/bridge/bridge.module#BridgeModule'},
          {path:'monster',
          canLoad:[BraveGuardService],
          canActivate:[BraveGuardService],
          loadChildren:'app/places/rabbit-cave/rabbit-cave.module#RabbitCaveModule'}
]
}
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
