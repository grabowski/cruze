import { Component,OnInit } from '@angular/core';
import {KnightService} from './knight/knight.service';
import {Knight} from './knight/knight.interface'
import {Router} from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Angular Router and Holy grail!';
  knights: Knight[];
  constructor(private knightService: KnightService,private router: Router){}
  ngOnInit(){
    this.knights = this.knightService.getKnights();
  }
  }
