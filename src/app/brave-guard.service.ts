import { Injectable } from '@angular/core';
import {CanLoad,CanActivate} from '@angular/router';
import {KnightService} from './knight/knight.service';
import {NotifyService} from './notify/notify.service';

@Injectable()
export class BraveGuardService implements CanLoad,CanActivate{

constructor(private knightsService:KnightService,private notify: NotifyService){

}
  canLoad(){
    return this.isBrave()

}
canActivate(){
      return this.isBrave()
}
isBrave(){
   if(this.knightsService.chosenKnight){

    return this.knightsService.chosenKnight.isBrave
    }

  return false
}
}
