import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifyComponent } from './notify.component';
import {NotifyService} from './notify.service'

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NotifyComponent],
  providers:[NotifyService]
})
export class NotifyModule { }
