import { Component, OnInit } from '@angular/core';
declare var Notification: any;

@Component({
  selector: 'app-notify',
  templateUrl: './notify.component.html',
  styleUrls: ['./notify.component.css']
})
export class NotifyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    Notification.requestPermission();
  }
  timSays(message:string){
     new Notification(message);

  }

}
