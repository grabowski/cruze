import { Injectable } from '@angular/core';
import{ BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class AppService {
  question: BehaviorSubject<string> = new BehaviorSubject(null);
  backToRoute: BehaviorSubject<string> = new BehaviorSubject(null);


}
