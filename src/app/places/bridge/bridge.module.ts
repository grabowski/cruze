import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { BridgeRoutingModule } from './bridge-routing.module';
import { BridgeComponent } from './bridge.component';
import {BridgeGuardService}from './bridge-guard.service';

@NgModule({
  imports: [
    CommonModule,
    BridgeRoutingModule
  ],
  exports:[BridgeRoutingModule],
  declarations: [BridgeComponent],
  providers:[BridgeGuardService]
})
export class BridgeModule { }
