import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BridgeComponent} from './bridge.component';
import {BridgeGuardService} from './bridge-guard.service';

const routes: Routes = [
  {
    path:'',
  component:BridgeComponent,
  canActivate:[BridgeGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BridgeRoutingModule { }
