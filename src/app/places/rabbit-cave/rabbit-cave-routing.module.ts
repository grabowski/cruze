import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RabbitCaveComponent} from './rabbit-cave.component';
import {RabbitGuardService} from './rabbit-guard.service';

const routes: Routes = [
  {path:'',
  component:RabbitCaveComponent,
  canDeactivate:[RabbitGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RabbitCaveRoutingModule { }
