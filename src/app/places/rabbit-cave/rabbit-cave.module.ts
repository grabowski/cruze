import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RabbitCaveRoutingModule } from './rabbit-cave-routing.module';
import { RabbitCaveComponent } from './rabbit-cave.component';
import {RabbitGuardService} from './rabbit-guard.service';

@NgModule({
  imports: [
    CommonModule,
    RabbitCaveRoutingModule
  ],
  declarations: [RabbitCaveComponent],
  providers:[RabbitGuardService]
})
export class RabbitCaveModule { }
