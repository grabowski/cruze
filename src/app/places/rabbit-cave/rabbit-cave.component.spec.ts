import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RabbitCaveComponent } from './rabbit-cave.component';

describe('RabbitCaveComponent', () => {
  let component: RabbitCaveComponent;
  let fixture: ComponentFixture<RabbitCaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RabbitCaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RabbitCaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
