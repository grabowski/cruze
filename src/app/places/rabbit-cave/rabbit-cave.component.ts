import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rabbit-cave',
  templateUrl: './rabbit-cave.component.html',
  styleUrls: ['./rabbit-cave.component.css']
})
export class RabbitCaveComponent implements OnInit {
  showDeath:boolean = false;

  constructor() { }

  ngOnInit() {
  }
  canLeave(){
      this.showDeath = true;
      return false
  }

}
