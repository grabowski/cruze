import {CanDeactivate} from '@angular/router';
import {RabbitCaveComponent} from './rabbit-cave.component';
export class RabbitGuardService implements CanDeactivate<RabbitCaveComponent>{
  canDeactivate(component: RabbitCaveComponent){
    return component.canLeave();

  }
}
