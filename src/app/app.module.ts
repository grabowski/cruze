import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KnightComponent } from './knight/knight.component';
import { KnightResolverService } from './knight/knight-resolver.service';
import { KnightService } from './knight/knight.service';
import { KnightsGuardService } from './knight/knights-guard.service';
import {BraveGuardService } from './brave-guard.service';
import {NotifyModule } from './notify/notify.module';
@NgModule({
  declarations: [
    AppComponent,
    KnightComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    NotifyModule
  ],
  providers: [KnightResolverService,KnightService,KnightsGuardService,BraveGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
