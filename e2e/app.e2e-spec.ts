import { PgsCruiseRoutingPage } from './app.po';

describe('pgs-cruise-routing App', () => {
  let page: PgsCruiseRoutingPage;

  beforeEach(() => {
    page = new PgsCruiseRoutingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
